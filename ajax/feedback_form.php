<?
define("BS_FORM_TYPE", 'CALL_ORDER'); //Тип почтового события
define("BS_FORM_TEMPLATE", 11); // Шаблон почтового события
define("NO_AGENT_CHECK", true);//Отключаем выполнение агентов, при выполнении данного скрипта
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
Loader::includeModule('iblock');

header('Conetent-type:application/json;charset=utf=8');

$name = $_POST['name'];
$phone = $_POST['phone'];

if(empty($name)){
  $data['response'] = "error";
  $data['content'] = "Введите имя";
}
elseif(!preg_match('/^[a-z]*$/i',$name)){
  $data['response'] = "error";
  $data['content'] = "Имя должен содержать только букви";
}
elseif(empty($phone)){
  $data['response'] = "error";
  $data['content'] = "Введите телефон";
}
elseif(!is_numeric($phone)){
  $data['response'] = "error";
  $data['content'] = "Телефон должен содержать только цифры";
}
elseif(iconv_strlen($phone) < 6){
  $data['response'] = "error";
  $data['content'] = "Телефон должен содержать не менее 6 символов";
}
elseif(iconv_strlen($phone) > 16){
  $data['response'] = "error";
  $data['content'] = "Телефон должен содержать не более 16 символов";
}


else{
  
  $arFields = array(
    "NAME" => $name,
    "PHONE" => $phone,
    "DATE_TIME" => date("d/m/Y G:i:s", time() + CTimeZone::GetOffset()) // Текущее время с учетом часового пояса
  );
  
  CEvent::Send(BS_FORM_TYPE, SITE_ID, $arFields, true, BS_FORM_TEMPLATE);
  
  $el = new CIBlockElement;
  
  $PROP = array();
  
  $PROP[13] = $phone;  
  
  $arLoadProductArray = Array(
    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
    "IBLOCK_ID"      => 6,
    "NAME" => $name,
    "PROPERTY_VALUES"=> $PROP,
    "ACTIVE"         => "Y",            // активен
    );
  
  $el->Add($arLoadProductArray);

  $data['response'] = "success";
  $data['content'] = "Спасибо, мы свяжемся с вами в ближайшее время";

}

echo json_encode($data);



  

