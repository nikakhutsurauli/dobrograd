<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("contact");
?>

<div class="contacts">
  <section class="contacts-persons">
    <div class="wrapper">
        <div class="h2">Контакты</div>
        <div class="contacts-persons__content">
            <div class="row mx-lg-0">
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-1.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Денис Антипов</div>
                            <div class="p">
                                Генеральный директор<br>
                                ООО «УК «Доброград-1»
                            </div>
                            <a href="mailto:antipovdv@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-2.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Елена  Устинова</div>
                            <div class="p">
                                Директор по маркетингу и PR
                                ООО&#160;«УК «Доброград-1»
                            </div>
                            <a href="mailto:UstinovaEA@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-3.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Алексей Истомин</div>
                            <div class="p">
                                Зам. генерального директора<br>
                                по взаимодействию с резидентами<br>
                                ООО «УК «Доброград-1»
                            </div>
                            <a href="mailto:istominai@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-4.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Олег Фомин</div>
                            <div class="p">
                                Вице-президент<br>
                                ГК «Аскона Лайф Групп»
                            </div>
                            <a href="mailto:FominOB@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-5.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Алексей Говырин</div>
                            <div class="p">
                                Член наблюдательного совета<br>
                                ОЭЗ «Доброград-1»,<br>
                                депутат ЗС Владимирской обл.
                            </div>
                            <a href="mailto:GovyrinAB@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 px-lg-0">
                    <div class="contacts-persons__item">
                        <div class="photo">
                            <img src="/assets/img/contacts/person-6.jpg" alt="">
                        </div>
                        <div class="info">
                            <div class="h4">Александр Ведяшкин</div>
                            <div class="p">
                                Главный инженер<br>
                                ООО «УК «Доброград-1»
                            </div>
                            <a href="mailto:VedyashkinAI@askonalife.com" class="mail">
                                <div class="icon">
                                    <svg width="32" height="32" fill="none">
                                        <use xlink:href="/assets/img/sprite.svg#mail"></use>
                                    </svg>
                                </div>
                                <span>Написать</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contacts-descr">
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="contacts-descr__info">
                    <div class="h2">Контакты</div>
                    <a href="tel:+7 915 790 04 77" class="phone">+7 915 790 04 77</a>
                    <a href="mailto:info@sezdobrograd.com" class="mail">info@sezdobrograd.com</a>
                    <div class="p">
                        ООО «Управляющая компания «Доброград-1»<br>
                        Юридический адрес:<br>
                        Россия, 601900, Владимирская область, Ковровский район, деревня Гороженово, Звездный бульвар, дом 1, помещение 20
                    </div>
                    <a href="tel:+7 915 790 04 77" class="button button_green">
                        <span>Позвонить</span>
                    </a>
                    <a href="" class="button transparent">
                        <span>Перезвонить мне</span>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-9">
                <div class="contacts-descr__ways">
                    <div class="h2">Как добраться</div>
                    <div class="contacts-ways">
                      <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "ways",
                            Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "N",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "N",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array("NAME","PREVIEW_TEXT","DETAIL_TEXT"),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "10",
                                "IBLOCK_TYPE" => "ways",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "20",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array("TIMETABLE","TITLE","TITLEDETAIL","SVG","TIMETABLESVG"),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N"
                            )
                        );?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>