<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

 <footer class="footer">
    <div class="footer__main">
        <div class="wrapper">
            <div class="row">
                <div class="col-12 col-lg-2">
                    <div class="footer__logo">
                        <svg width="108" height="61" fill="none">
                            <use xlink:href="/assets/img/sprite.svg#logo-white"></use>
                        </svg>
                    </div>
                </div>
                <div class="col-12 col-lg-7">
                    <div class="footer__nav">
                        <div class="row">
                            <div class="col-12 col-lg-3">
                                <a href="">Информация</a>
                                <ul>
                                    <li><a href="">О проекте</a></li>
                                    <li><a href="/#territory-map">Мастер-план ОЭЗ</a></li>
                                    <li><a href="">Преимущества территории</a></li>
                                    <li><a href="">Контакты</a></li>
                                    <li><a href="">Как добраться</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-4">
                                <a href="">Инфраструктура</a>
                                <ul>
                                    <li><a href="">Социальная</a></li>
                                    <li><a href="">Для бизнеса</a></li>
                                    <li><a href="">Транспортная</a></li>
                                    <li><a href="">Ресурсные возможности</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-lg-5">
                                <a href="">Инвестору</a>
                                <ul>
                                    <li><a href="">Льготы и преференции</a></li>
                                    <li><a href="">Управляющая компания</a></li>
                                    <li><a href="">Строительство «под ключ»</a></li>
                                    <li><a href="">Кадровое агентство</a></li>
                                    <li><a href="">Путь инвестора</a></li>
                                    <li><a href="">Требования к резидентам</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="footer__connection">
                        <a href="" class="button button_green">
                            <svg width="17" height="16" fill="none">
                                <use xlink:href="/assets/img/sprite.svg#download"></use>
                            </svg>
                            <span>Скачать презентацию</span>
                        </a>
                        <a href="" class="button button_green">
                            <svg width="17" height="16" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#download"></use>
                            </svg>
                            <span>Download presentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer__sub">
        <div class="wrapper">
            <div class="row">
                <div class="col-6 col-lg-4">
                    <div class="copyright">© 2021, Доброград</div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="policy">
                       <a href="">политика конфиденциальности</a> 
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="basmanov">
                        <a href="https://basmanov.ru/" target="_blank">
                            <svg width="181" height="12" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#basmanov"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- перенести куда надо и потом удалить файл -->
    <div class="pop-ups">
        <form style="display: none;" id="call-request" class="pop-up">
            <div class="h2 pop-up__title">Вам перезвонить?</div>
            <div class="pop-up__descr">Наш специалист поможет Вам разобраться с любыми вопросами.</div>
            <span class="response"></span>
            <div class="pop-up__area">
                <div class="pop-up__field">
                    <input id="name" type="text" name="name">
                    <p for="name">Имя (необязательно)</p>
                </div>
                <div class="pop-up__field">
                    <input type="tel" id="phone" name="phone">
                    <p>Телефон</p>
                </div>
            </div>
            <button class="button button_green"><span>Позвонить мне</span></button>
            <div class="pop-up__privacy">
                Нажимая на кнопку, вы даете свое согласие на <a href="" target="_blank">обработку ваших персональных
                    данных</a>
            </div>
        </form>
        <div style="display: none" id="steps-picture" class="pop-up">
            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/steps.jpg" alt="">
        </div>

        <form style="display: none;" id="resident-request" class="pop-up">
            <div class="h2 pop-up__title">Стать резидентом</div>
            <div class="pop-up__area">
                <div class="pop-up__field">
                    <input type="text" name="name">
                    <p for="name">Имя (необязательно)</p>
                </div>
                <div class="pop-up__field">
                    <input type="text" name="tel">
                    <p>Телефон</p>
                </div>
            </div>
            <button class="button button_green"><span>Стать резидентом</span></button>
            <div class="pop-up__privacy">
                Нажимая на кнопку, вы даете свое согласие на <a href="" target="_blank">обработку ваших персональных
                    данных</a>
            </div>
        </form>

        <form style="display: none;" id="consult-request" class="pop-up">
            <div class="h2 pop-up__title">Получить консультацию</div>
            <div class="pop-up__area">
                <div class="pop-up__field">
                    <input type="text" name="name">
                    <p for="name">Имя (необязательно)</p>
                </div>
                <div class="pop-up__field">
                    <input type="text" name="tel">
                    <p>Телефон</p>
                </div>
            </div>
            <button class="button button_green"><span>Получить консультацию</span></button>
            <div class="pop-up__privacy">
                Нажимая на кнопку, вы даете свое согласие на <a href="" target="_blank">обработку ваших персональных
                    данных</a>
            </div>
        </form>

        <form style="display: none;" id="tarifs-request" class="pop-up">
            <div class="h2 pop-up__title">Узнать тарифы</div>
            <div class="pop-up__area">
                <div class="pop-up__field">
                    <input type="text" name="name">
                    <p for="name">Имя (необязательно)</p>
                </div>
                <div class="pop-up__field">
                    <input type="text" name="tel">
                    <p>Телефон</p>
                </div>
            </div>
            <button class="button button_green"><span>Узнать тарифы</span></button>
            <div class="pop-up__privacy">
                Нажимая на кнопку, вы даете свое согласие на <a href="" target="_blank">обработку ваших персональных
                    данных</a>
            </div>
        </form>

        <form style="display: none;" id="route-info" class="pop-up">
            <div class="pop-up__accardion">
                <div class="acc-item active">
                    <div class="acc-item__headline" data-toggle="route-item">
                        <svg width="31" height="31">
                            <circle class="circle-dot" cx="15.2578" cy="15.5" r="4"></circle>
                            <g fill="none">
                                <circle class="circle-path-1" transform="rotate(-90 15.5 15.5)" cx="15.5" cy="15.5" r="15"></circle>
                            </g>
                        </svg>
                        <p>На автомобиле</p>
                    </div>
                    <div class="acc-item__content active">
                        <span>Прямое сообщение «Трасса М7 – Доброград»</span>
                        <p>
                            Москва - ОЭЗ Доброград — 240 км.<br>
                            Владимир - ОЭЗ Доброград — 45 км.<br>
                            Ковров - ОЭЗ Доброград — 12 км.
                        </p>
                        <a href="https://yandex.ru/maps/?from=api-maps&amp;ll=41.242649%2C56.214916&amp;mode=routes&amp;origin=jsapi_2_1_74&amp;rtext=~56.220204%2C41.172766&amp;rtt=auto&amp;ruri=~&amp;z=12" target="_blank">Построить маршрут</a>
                    </div>
                </div>

                <div class="acc-item">
                    <div class="acc-item__headline" data-toggle="route-item">
                        <svg width="31" height="31">
                            <circle class="circle-dot" cx="15.2578" cy="15.5" r="4"></circle>
                            <g fill="none">
                                <circle class="circle-path-1" transform="rotate(-90 15.5 15.5)" cx="15.5" cy="15.5" r="15"></circle>
                            </g>
                        </svg>
                        <p>На скоростном поезде</p>
                    </div>
                    <div class="acc-item__content">
                        <span>Прямое сообщение, без пересадок</span>
                        <p>
                            Высокоскоростные поезда<br>
                            «Ласточка» и «Стриж».
                        </p>
                        <span>Время в пути:</span>
                        <p>Москва - Ковров — 2 часа 20 минут</p>
                        <p>Возможна услуга трансфера с вокзала</p>
                        <a href="https://rasp.yandex.ru/search/?fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&amp;toName=%D0%9A%D0%BE%D0%B2%D1%80%D0%BE%D0%B2&amp;toId=c10664&amp;when=%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F&amp;transport=train&amp;seats=y&amp;highSpeedTrain=252&amp;highSpeedTrain=260" target="_blank">Узнать расписание поездов</a>
                    </div>
                </div>

                <div class="acc-item">
                    <div class="acc-item__headline" data-toggle="route-item">
                        <svg width="31" height="31">
                            <circle class="circle-dot" cx="15.2578" cy="15.5" r="4"></circle>
                            <g fill="none">
                                <circle class="circle-path-1" transform="rotate(-90 15.5 15.5)" cx="15.5" cy="15.5" r="15"></circle>
                            </g>
                        </svg>
                        <p>На общественном транспорте</p>
                    </div>
                    <div class="acc-item__content">
                        <p>Отправление из г. Ковров от автовокзала.</p>
                        <p>Отправление из г. Доброград от «КПП»</p>
                        <span>Маршрут:</span>
                        <p>Ковров — Мелехово — Доброград</p>
                        <p>Время в пути: 20 мин</p>
                        <a href="https://gorod-dobrograd.ru/schedule_bus.pdf" target="_blank">Узнать расписание автобусов</a>
                    </div>
                </div>

                <div class="acc-item">
                    <div class="acc-item__headline" data-toggle="route-item">
                        <svg width="31" height="31">
                            <circle class="circle-dot" cx="15.2578" cy="15.5" r="4"></circle>
                            <g fill="none">
                                <circle class="circle-path-1" transform="rotate(-90 15.5 15.5)" cx="15.5" cy="15.5" r="15"></circle>
                            </g>
                        </svg>
                        <p>На малой авиации</p>
                    </div>
                    <div class="acc-item__content">
                        <p>Вертолеты и самолеты</p>
                        <p>
                            На территории Доброграда действует <br>
                            аэродром для принятия малой авиации.
                        </p>
                        <span>Координаты GPS:</span>
                        <p>N56°14,196’, E41°14,127’</p>
                        <p>Индекс УУИГ/UUIG</p>
                    </div>
                </div>

            </div>
        </form>
    </div>
</footer>
	<?php
	use Bitrix\Main\Page\Asset;
    Asset::getInstance()->addjs(SITE_TEMPLATE_PATH .'/assets/js/libs.js?v=1c7c1a67c4c059f49035');
    Asset::getInstance()->addjs(SITE_TEMPLATE_PATH .'/assets/js/main.js?v=d65c15f11d1ef7917dd5');
    Asset::getInstance()->addstring('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>');
    Asset::getInstance()->addjs(SITE_TEMPLATE_PATH .'/assets/js/script.js');
    ?>
</body>
</html>