/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		1: 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([4,0]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _node_modules_fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _node_modules_fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_fancyapps_fancybox_dist_jquery_fancybox_min_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_svgxuse_svgxuse_min_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6);
/* harmony import */ var _node_modules_svgxuse_svgxuse_min_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_svgxuse_svgxuse_min_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_js___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7);
/* harmony import */ var _assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(11);
/* harmony import */ var _assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_main_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_css_main_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(14);
/* harmony import */ var _assets_css_main_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_css_main_css__WEBPACK_IMPORTED_MODULE_4__);
// JS 
window.jQuery = window.$ = $;


 // SCSS

 // CSS 


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1)))

/***/ }),
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, AOS, Swiper, Parallax) {/* harmony import */ var _panzoom_panzoom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);

$(document).ready(function () {
  AOS.init({
    disable: false,
    startEvent: 'DOMContentLoaded',
    initClassName: 'aos-init',
    animatedClassName: 'aos-animate',
    useClassNames: false,
    disableMutationObserver: false,
    debounceDelay: 50,
    throttleDelay: 99,
    offset: 120,
    delay: 0,
    duration: 400,
    easing: 'ease',
    once: false,
    mirror: false,
    anchorPlacement: 'top-bottom'
  });

  var goToAnchorPage = function goToAnchorPage() {
    var windowHash = window.location.hash;

    if (windowHash === "#territory-map") {
      $('[data-toggle="map"]').removeClass('active');
      $('.index-map__title').removeClass('active');
      $('.index-map__maps div').removeClass('active');
      $('[data-show="territory"]').addClass('active');
      $("#territory-title").addClass('active');
      $("#territory-map").addClass('active');
    }

    ;
  };

  goToAnchorPage();
  $('body').on('mouseenter', '.button', function (e) {
    var offset = $(this).offset();
    $(this).css('--x', "".concat(e.pageX - offset.left, "px"));
    $(this).css('--y', "".concat(e.pageY - offset.top, "px"));
  });
  $('body').on('mouseleave', '.button', function (e) {
    var offset = $(this).offset();
    $(this).css('--x', "".concat(e.pageX - offset.left, "px"));
    $(this).css('--y', "".concat(e.pageY - offset.top, "px"));
  });
  $('body').on('mousemove', '.button_green, .button_grey, .activity-item, .news-main-item', function (e) {
    var offset = $(this).offset();
    $(this).css('--x', "".concat(e.pageX - offset.left, "px"));
    $(this).css('--y', "".concat(e.pageY - offset.top, "px"));
  });
  var advendagesSlider = new Swiper('.advendages-slider__slider', {
    slidesPerView: 'auto',
    spaceBetween: 10
  });
  var aboutMapsSlider = new Swiper('.about-maps__slider', {
    slidesPerView: '1',
    allowTouchMove: false,
    navigation: {
      nextEl: '.about-maps__arrow.next',
      prevEl: '.about-maps__arrow.prev'
    },
    on: {
      init: function init() {
        var currentSlide = this.activeIndex + 1;
        var slidesAmount = this.slides.length;

        if (currentSlide === 1) {
          $('.about-maps__arrow.prev').addClass('disabled');
        } else {
          $('.about-maps__arrow.prev').removeClass('disabled');
        }

        ;

        if (currentSlide === slidesAmount) {
          $('.about-maps__arrow.next').addClass('disabled');
        } else {
          $('.about-maps__arrow.next').removeClass('disabled');
        }

        ;
      },
      slideChange: function slideChange() {
        var currentSlide = this.activeIndex + 1;
        var slidesAmount = this.slides.length;

        if (currentSlide === 1) {
          $('.about-maps__arrow.prev').addClass('disabled');
        } else {
          $('.about-maps__arrow.prev').removeClass('disabled');
        }

        ;

        if (currentSlide === slidesAmount) {
          $('.about-maps__arrow.next').addClass('disabled');
        } else {
          $('.about-maps__arrow.next').removeClass('disabled');
        }

        ;
      }
    }
  });
  var aboutMapsSubslider = new Swiper('.about-maps__subslider', {
    slidesPerView: '1',
    spaceBetween: 20,
    allowTouchMove: false,
    autoHeight: true,
    navigation: {
      nextEl: '.about-maps__arrow.next',
      prevEl: '.about-maps__arrow.prev'
    }
  });
  var infrastructureMapSlider = new Swiper('.infrastructure-map__slider', {
    slidesPerView: 'auto',
    spaceBetween: 8,
    freeMode: true,
    navigation: {
      nextEl: Array.prototype.slice.call(document.querySelectorAll('.infrastructure-map .next')),
      prevEl: Array.prototype.slice.call(document.querySelectorAll('.infrastructure-map .prev'))
    },
    breakpoints: {
      576: {
        spaceBetween: 18
      }
    }
  });
  var investorAdvantagesSlider = new Swiper('.investor-advantages__slider', {
    slidesPerView: 1,
    spaceBetween: 16,
    freeMode: true,
    navigation: {
      nextEl: Array.prototype.slice.call(document.querySelectorAll('.investor-advantages .next')),
      prevEl: Array.prototype.slice.call(document.querySelectorAll('.investor-advantages .prev'))
    },
    breakpoints: {
      768: {
        slidesPerView: 'auto'
      }
    }
  });
  var investorRezidentSlider = new Swiper('.investor-rezident__slider', {
    slidesPerView: 1,
    spaceBetween: 16,
    freeMode: false,
    navigation: {
      nextEl: Array.prototype.slice.call(document.querySelectorAll('.investor-rezident .next')),
      prevEl: Array.prototype.slice.call(document.querySelectorAll('.investor-rezident .prev'))
    },
    breakpoints: {
      768: {
        slidesPerView: 'auto'
      }
    }
  });
  var infrastructureTransportMapInfo = new Swiper('.infrastructure-transport__map-info', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    allowTouchMove: false,
    breakpoints: {
      577: {
        spaceBetween: 24,
        allowTouchMove: true
      }
    }
  });
  var newsDetailsSlider = new Swiper('.news-details-content__slider .swiper-container', {
    slidesPerView: '1',
    spaceBetween: 20,
    loop: true,
    navigation: {
      nextEl: '.news-details-content__slider .next',
      prevEl: '.news-details-content__slider .prev'
    }
  });
  $('body').on('change', 'input', function () {
    if ($(this).val() !== '') {
      $(this).addClass('full');
    } else {
      $(this).removeClass('full');
    }

    ;
  });
  $('body').on('click', '[data-toggle="mobile-menu"]', function () {
    $(this).toggleClass('active');
    $('.header__content').toggleClass('active');
    $('.mobile-menu').toggleClass('active');
  });
  $('body').on('click', '[data-toggle="map"]', function () {
    var activeMap = $(this).data('show');
    $('.index-map__maps div').removeClass('active');
    $('[data-toggle="map"]').removeClass('active');
    $('.index-map__title').removeClass('active');
    $("#".concat(activeMap, "-title")).addClass('active');
    $("#".concat(activeMap, "-map")).addClass('active');
    $(this).addClass('active');
  });
  $('body').on('click', '[data-toggle="contacts-way"]', function () {
    var activeWay = $(this).data('way');
    $('.contacts-ways__tab').removeClass('active');
    $('.contacts-ways__descr').removeClass('active');
    $("#".concat(activeWay)).addClass('active');
    $(this).addClass('active');

    if ($(window).width() < 778) {
      $('.contacts-ways__descr').slideUp();
      $(this).next('.contacts-ways__descr').slideDown();
    }
  });
  $('body').on('click', '[data-toggle="route-item"]', function () {
    $('.acc-item').removeClass('active');
    $('.acc-item__content').slideUp();
    $(this).parent('.acc-item').addClass('active');
    $(this).next('.acc-item__content').slideDown();
  });
  $('body').on('click', '[data-type="anchor"]', function (e) {
    e.preventDefault();
    var section = $(this).attr("href");
    $('html,body').animate({
      scrollTop: $(section).offset().top
    }, 500);
  });
  $('body').on('click', '.toggle', function () {
    $(this).toggleClass('active');
  });

  if ($(window).width() <= 992) {
    $('.footer__nav ul').prev('a').attr('data-toggle', 'footer-submenu');
    $('body').on('click', '[data-toggle="footer-submenu"]', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $(this).next('ul').slideToggle();
    });
  }

  ;

  var showMoreTextMobile = function showMoreTextMobile() {
    var screenWidth = $(window).width();

    if (screenWidth < 768) {
      $('[data-show="text"]').prev().hide();
    }

    $('body').on('click', '[data-show="text"]', function () {
      $(this).toggleClass('active');
      $(this).prev().slideToggle();
    });
  };

  showMoreTextMobile();
  var aboutAdvantagesSlider = undefined;

  function initAboutAdvantagesSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 769 && aboutAdvantagesSlider == undefined) {
      aboutAdvantagesSlider = new Swiper('.about-advantages__slider', {
        slidesPerView: 'auto',
        spaceBetween: 20
      });
    } else if (screenWidth > 768 && aboutAdvantagesSlider != undefined) {
      aboutAdvantagesSlider.destroy();
      aboutAdvantagesSlider = undefined;
      $('.about-advantages__slider .swiper-wrapper').removeAttr('style');
      $('.about-advantages__slider .swiper-slide').removeAttr('style');
    }
  }

  ;
  initAboutAdvantagesSlider();
  var aboutRegionSlider = undefined;

  function initAboutRegionSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 769 && aboutRegionSlider == undefined) {
      aboutRegionSlider = new Swiper('.about-region__slider', {
        slidesPerView: 'auto',
        spaceBetween: 20
      });
    } else if (screenWidth > 768 && aboutRegionSlider != undefined) {
      aboutRegionSlider.destroy();
      aboutRegionSlider = undefined;
      $('.about-region__slider .swiper-wrapper').removeAttr('style');
      $('.about-region__slider .swiper-slide').removeAttr('style');
    }
  }

  ;
  initAboutRegionSlider();
  var aboutPotentialSlider = undefined;

  function initAboutPotentialSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 900 && aboutPotentialSlider == undefined) {
      aboutPotentialSlider = new Swiper('.about-potential__slider', {
        slidesPerView: 'auto'
      });
    } else if (screenWidth > 899 && aboutPotentialSlider != undefined) {
      aboutPotentialSlider.destroy();
      aboutPotentialSlider = undefined;
      $('.about-potential__slider .swiper-wrapper').removeAttr('style');
      $('.about-potential__slider .swiper-slide').removeAttr('style');
    }
  }

  ;
  initAboutPotentialSlider();
  var infrastructureAdvantageslSlider = undefined;

  function initInfrastructureAdvantageslSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 768 && infrastructureAdvantageslSlider == undefined) {
      infrastructureAdvantageslSlider = new Swiper('.infrastructure-advantages__slider', {
        slidesPerView: 'auto',
        spaceBetween: 20
      });
    } else if (screenWidth > 767 && infrastructureAdvantageslSlider != undefined) {
      infrastructureAdvantageslSlider.destroy();
      infrastructureAdvantageslSlider = undefined;
      $('.infrastructure-advantages__slider .swiper-wrapper').removeAttr('style');
      $('.infrastructure-advantages__slider .swiper-slide').removeAttr('style');
    }
  }

  ;
  initInfrastructureAdvantageslSlider();
  var infrastructureSocialSlider = undefined;

  function initInfrastructureSocialSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 768 && infrastructureSocialSlider == undefined) {
      infrastructureSocialSlider = new Swiper('.infrastructure-social__list', {
        slidesPerView: 'auto',
        autoHeight: true,
        spaceBetween: 12
      });
    } else if (screenWidth > 767 && infrastructureSocialSlider != undefined) {
      infrastructureSocialSlider.destroy();
      infrastructureSocialSlider = undefined;
      $('.infrastructure-social__list .swiper-wrapper').removeAttr('style');
      $('.infrastructure-social__list .swiper-slide').removeAttr('style');
    }
  }

  ;
  initInfrastructureSocialSlider();
  var infrastructureBusinessListSlider = undefined;

  function initInfrastructureBusinessListSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 768 && infrastructureBusinessListSlider == undefined) {
      infrastructureBusinessListSlider = new Swiper('.infrastructure-business__list', {
        slidesPerView: 'auto',
        spaceBetween: 20,
        navigation: {
          nextEl: Array.prototype.slice.call(document.querySelectorAll('.infrastructure-business__list .next')),
          prevEl: Array.prototype.slice.call(document.querySelectorAll('.infrastructure-business__list .prev'))
        }
      });
    } else if (screenWidth > 767 && infrastructureBusinessListSlider != undefined) {
      infrastructureBusinessListSlider.destroy();
      infrastructureBusinessListSlider = undefined;
      $('.infrastructure-business__list .swiper-wrapper').removeAttr('style');
      $('.infrastructure-business__list .swiper-slide').removeAttr('style');
    }
  }

  ;
  initInfrastructureBusinessListSlider();
  var investorBuiSlider = undefined;

  function initInvestorBuiSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 992 && investorBuiSlider == undefined) {
      investorBuiSlider = new Swiper('.investor-buil__slider', {
        slidesPerView: 'auto',
        spaceBetween: 24,
        navigation: {
          nextEl: Array.prototype.slice.call(document.querySelectorAll('.investor-buil__slider .next')),
          prevEl: Array.prototype.slice.call(document.querySelectorAll('.investor-buil__slider .prev'))
        }
      });
    } else if (screenWidth > 991 && investorBuiSlider != undefined) {
      investorBuiSlider.destroy();
      investorBuiSlider = undefined;
      $('.investor-buil__slider .swiper-wrapper').removeAttr('style');
      $('.investor-buil__slider .swiper-slide').removeAttr('style');
    }
  }

  ;
  initInvestorBuiSlider();
  var investorBuiInfoSlider = undefined;

  function initInvestorBuiInfoSlider() {
    var screenWidth = $(window).width();

    if (screenWidth < 992 && investorBuiInfoSlider == undefined) {
      investorBuiInfoSlider = new Swiper('.investor-buil__list', {
        slidesPerView: 'auto',
        spaceBetween: 12,
        navigation: {
          nextEl: Array.prototype.slice.call(document.querySelectorAll('.investor-buil__slider .next')),
          prevEl: Array.prototype.slice.call(document.querySelectorAll('.investor-buil__slider .prev'))
        }
      });
    } else if (screenWidth > 991 && investorBuiInfoSlider != undefined) {
      investorBuiInfoSlider.destroy();
      investorBuiInfoSlider = undefined;
      $('.investor-buil__list .swiper-wrapper').removeAttr('style');
      $('.investor-buil__list .swiper-slide').removeAttr('style');
    }
  }

  ;
  initInvestorBuiInfoSlider();

  var counterAnimation = function counterAnimation() {
    var topPoint = $(window).scrollTop() + window.innerHeight;

    if ($('.about-region__statistic').offset() != undefined) {
      var elemetPoint = $('.about-region__statistic').offset().top;

      if (topPoint > elemetPoint) {
        $('[data-animation="counter"]').each(function () {
          var count = $(this).data('count');
          $(this).prop('counter', 0).animate({
            counter: count
          }, {
            duration: 2000,
            easing: 'swing',
            step: function step(now) {
              if (Number.isInteger(count)) {
                now = now.toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').toString();
                $(this).text(now.replace(".", ","));
              } else {
                now = now.toFixed(1).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').toString();
                $(this).text(now.replace(".", ","));
              }
            }
          });
        });
      }

      ;
    }

    ;
  };

  counterAnimation();

  var stepsAnimation = function stepsAnimation() {
    var topPoint = $(window).scrollTop() + window.innerHeight;

    if ($('.investor-steps__content').offset() != undefined) {
      var elemetPoint = $('.investor-steps__content').offset().top + 500;
      var circles = $('[data-animation="steps"]').find('.circle');
      var circlesText = $('[data-animation="steps"]').find('.text');

      if (topPoint > elemetPoint) {
        var currentStepSpeed = 100;
        setTimeout(function () {
          $('[data-animation="steps"]').find('.line').addClass('active');
        }, 50);
        circles.each(function (index) {
          var currentStepItem = $(this);
          setTimeout(function () {
            currentStepItem.addClass('active');
          }, currentStepSpeed * (index + 1));
        });
        circlesText.each(function (index) {
          var currentStepItem = $(this);
          setTimeout(function () {
            currentStepItem.addClass('active');
          }, currentStepSpeed * (index + 1));
        });
      }

      ;
    }

    ;
  };

  stepsAnimation();

  var engineeringListAnimation = function engineeringListAnimation() {
    var topPoint = $(window).scrollTop() + window.innerHeight;

    if ($('.infrastructure-engineering__list.desktop').offset() != undefined) {
      var elemetPoint = $('.infrastructure-engineering__list.desktop').offset().top;
      var circles = $('[data-animation="steps"]').find('.circle');

      if (topPoint > elemetPoint) {
        var currentStepSpeed = 100;
        setTimeout(function () {
          $('[data-animation="steps"]').find('.line').addClass('active');
        }, 50);
        circles.each(function (index) {
          var currentStepItem = $(this);
          setTimeout(function () {
            currentStepItem.addClass('active');
          }, currentStepSpeed * (index + 1));
        });
      }

      ;
    }

    ;
  };

  engineeringListAnimation();

  var investorTaxSlider = function investorTaxSlider() {
    var choisedNavSlider = undefined;
    var choisedSlider = undefined;

    var initSlider = function initSlider(sliderID) {
      if (choisedNavSlider !== undefined) {
        choisedNavSlider.destroy();
        choisedNavSlider = undefined;
        $("".concat(sliderID, " .navigation .swiper-wrapper")).removeAttr('style');
        $("".concat(sliderID, " .navigation .swiper-slide")).removeAttr('style');
      }

      if (choisedSlider !== undefined) {
        choisedSlider.destroy();
        choisedSlider = undefined;
        $("".concat(sliderID, " .slider .swiper-wrapper")).removeAttr('style');
        $("".concat(sliderID, " .slider .swiper-slide")).removeAttr('style');
      }

      choisedNavSlider = new Swiper("".concat(sliderID, " .navigation"), {
        slidesPerView: 'auto',
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        allowTouchMove: true,
        freeMode: true,
        breakpoints: {
          1000: {
            allowTouchMove: false
          }
        }
      });
      choisedSlider = new Swiper("".concat(sliderID, " .slider"), {
        slidesPerView: 1,
        thumbs: {
          swiper: choisedNavSlider
        }
      });
    };

    initSlider('#tax-incentives');
    $('body').on('click', '[data-toggle="investor-tax-slider"]', function () {
      $(this).prev('.p').toggleClass('active');
      $(this).next('.p').toggleClass('active');
      $('.investor-tax-sliders__slider').removeClass('active');
      $('.investor-privileges__descr').removeClass('active');
      var choisedSlider = $(this).parent().find('.p.active').data('slider');
      var choisedSliderID = '#' + choisedSlider;
      $(choisedSliderID).addClass('active');
      $(".".concat(choisedSlider, "-text")).addClass('active');
      initSlider(choisedSliderID);
    });
  };

  investorTaxSlider();
  $(window).on('resize', function () {
    initAboutAdvantagesSlider();
    initAboutRegionSlider();
    initAboutPotentialSlider();
    initInfrastructureAdvantageslSlider();
    initInvestorBuiSlider();
    initInfrastructureBusinessListSlider();
  });
  $(document).scroll(function () {
    counterAnimation();
    stepsAnimation();
    engineeringListAnimation();
  });
  var parallaxInstance = undefined;

  var parallaxInit = function parallaxInit() {
    var screenWidth = $(window).width();
    var scene = document.getElementById('scene');

    if (screenWidth > 899 && scene !== null) {
      var _parallaxInstance = new Parallax(scene);
    } else if (scene !== null) {
      var _parallaxInstance2 = undefined;
      $('#scene').removeAttr('style');
      $('#scene .swiper-slide').removeAttr('style');
    }
  };

  parallaxInit();

  var initInfrastructureMapMove = function initInfrastructureMapMove() {
    var elem = document.getElementById('map-move');

    if (elem !== null) {
      var panzoom = Object(_panzoom_panzoom__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(elem, {
        minScale: 0.5,
        maxScale: 15
      });
      document.getElementById('maps-zoomin').addEventListener('click', panzoom.zoomIn);
      document.getElementById('maps-zoomout').addEventListener('click', panzoom.zoomOut);
    }
  };

  initInfrastructureMapMove();

  var initInfrastructureMapTransportMove = function initInfrastructureMapTransportMove() {
    var elem = document.getElementById('map-transport');

    if (elem !== null) {
      var panzoom = Object(_panzoom_panzoom__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(elem, {
        minScale: 0.5,
        maxScale: 15
      });
      document.getElementById('transport-zoomin').addEventListener('click', panzoom.zoomIn);
      document.getElementById('transport-zoomout').addEventListener('click', panzoom.zoomOut);
    }
  };

  initInfrastructureMapTransportMove();

  var initResidentsMapMove = function initResidentsMapMove() {
    var elem = document.getElementById('map-residents');

    if (elem !== null) {
      var panzoom = Object(_panzoom_panzoom__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"])(elem, {
        minScale: 0.5,
        maxScale: 15
      });
      document.getElementById('residents-zoomin').addEventListener('click', panzoom.zoomIn);
      document.getElementById('residents-zoomout').addEventListener('click', panzoom.zoomOut);
    }
  };

  initResidentsMapMove();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1), __webpack_require__(8), __webpack_require__(9), __webpack_require__(10)))

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(12);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(3)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(15);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(3)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })
/******/ ]);