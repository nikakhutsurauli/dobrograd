
$(function(){
 var form = $('#call-request');

 form.submit(function(e){
     e.preventDefault();

     $.ajax({
       type: 'POST',
       url: '/ajax/feedback_form.php',
       data: form.serialize(),
       dataType: 'json',
       success: function(data){
           if(data.response == "success"){
            $('.response').text(data.content).css({ 'color': 'green', 'font-size': '15px', 'padding-top': '15px'});
           }
           if(data.response == "error"){
            $('.response').text(data.content).css({ 'color': 'red', 'font-size': '15px', 'padding-top': '15px'});
          }
          console.log(data);
       },    
     });
 });
});
 