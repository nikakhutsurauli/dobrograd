<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
 use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?$APPLICATION->ShowTitle();?></title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH .'/assets/css/libs.min.css?v=31cdd905aa4787e2049f');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH .'/assets/css/main.min.css?v=31cdd905aa4787e2049f');
    ?>
    <?$APPLICATION->ShowHead();?>
</head>
<body>
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>
<header class="header header_transparent">
    <div class="header__content">
        <div class="wrapper">
            <div class="row align-items-center justify-content-between">
                <div class="col-auto col-lg-2">
                    <a href="/" class="header__logo">
                        <svg width="90" height="48">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#logo-lonley"></use>
                        </svg>
                    </a>
                </div>
                <div class="col-auto col-lg-7">
                    <nav class="navbar">
                        <a href="/">о проекте</a>
                        <a href="/investor.html">инвестору</a>
                        <a href="/infrastructure.html">инфраструктура</a>
                        <a href="/news">новости</a>
                        <a href="/residents.html">резиденты</a>
                        <a href="/contact">контакты</a>
                    </nav>
                </div>
                <div class="col-auto col-lg-3">
                    <div class="header__connection">
                        <a href="tel:+" class="tel">8 800 600 80 50</a>
                        <a href="" data-fancybox data-animation-duration="700" data-src="#call-request" href="javascript:;">заказать обратный звонок</a>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="burger" data-toggle="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="wrapper">
            <div class="mobile-menu__list">
                <a href="/">о проекте</a>
                <a href="/investor.html">инвестору</a>
                <a href="/infrastructure.html">инфраструктура</a>
                <a href="/news.html">новости</a>
                <a href="/residents.html">резиденты</a>
                <a href="/contacts.html">контакты</a>
            </div>
        </div>
    </div>
</header>




