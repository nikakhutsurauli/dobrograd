<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="news-main">
  <div class="wrapper">
  <div class="news-main__list">
	<?foreach ($arResult['ITEMS'] as $arItem): ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="news-main__group" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" class="news-main-item">
		<? if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])) : ?>
			<div class="news-main-item__picture" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
		<? endif; ?>
			<div class="news-main-item__title"><?=$arItem["NAME"]?></div>
		</a>
	</div>
	<? endforeach; ?>
	<div class="news-main__group">
		<div class="news-main-form">
			<div class="h2">Подпишитесь на обновления</div>
			<div class="p">Получайте еженедельные обновления о последних событиях особой экономической зоны.</div>

			<form class="form">
				<div class="form__area">
					<div class="form__field">
						<input type="email" name="email">
						<p>E-mail</p>
					</div>
				</div>
				<button class="button button_green"><span>Подписаться</span></button>
				<div class="form__privacy">
					Нажимая на кнопку, вы даете свое согласие на <a href="/news-details.html" target="_blank">обработку ваших персональных данных</a>
				</div>
			</form>
		</div>  
	</div>
  </div>
</div>
</section>