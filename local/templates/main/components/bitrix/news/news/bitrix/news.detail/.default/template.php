<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="news-details-content">
  <div class="wrapper wrapper_samll">
    <div class="news-detail">   	
      <div class="news-detail">
        <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
            <h3><?=$arResult["NAME"]?></h3>
        <?endif;?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
        <?endif;?>
        <?if($arResult["NAV_RESULT"]):?>
            <?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
            <?echo $arResult["NAV_TEXT"];?>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
        <?elseif($arResult["DETAIL_TEXT"] <> ''):?>
            <?echo $arResult["DETAIL_TEXT"];?>
        <?else:?>
            <p><?echo $arResult["PREVIEW_TEXT"];?></p>
        <?endif?>
        <div style="clear:both"></div>
        <?foreach($arResult["FIELDS"] as $code=>$value):
            if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
            {?>
                <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
                    </div>
                </div>
                </div>
            <?}?>
        <?endforeach;?>
    </div>
    <section class="news-details-headline">
        <div class="wrapper wrapper_samll">
            <a href="<?=$arResult["LIST_PAGE_URL"]?>"><?=GetMessage("RETURN_TO_LIST")?></a>
            <div class="h2 green">Резидентов особой экономической зоны «Доброград» подключают к газу</div>
            <div class="date">10 декабря 2020</div>
        </div>
    </section>
    <div class="navigation">
        <div class="navigation__arrow prev">
            <svg width="20" height="20">
                <use xlink:href="/assets/img/sprite.svg#arrow-left"></use>
            </svg>
        </div>
        <div class="navigation__arrow next">
            <svg width="20" height="20">
                <use xlink:href="/assets/img/sprite.svg#arrow-right"></use>
            </svg>
        </div>
    </div>
    <section class="news-details-form">
    <div class="wrapper">
        <div class="news-details-form__content">
            <div class="h2">Подпишитесь на обновления</div>
            <div class="p">Получайте еженедельные обновления о последних событиях особой экономической зоны.</div>
            <form class="form">
                <div class="form__field">
                    <input type="email" name="email">
                    <p>E-mail</p>
                </div>
                <button class="button button_green"><span>Подписаться</span></button>
                <div class="form__privacy">
                    Нажимая на кнопку, вы даете свое согласие на <a href="" target="_blank">обработку ваших персональных данных</a>
                </div>
            </form>
        </div>
    </div>
    </section>
   </div>
  </div>
</section>  


                