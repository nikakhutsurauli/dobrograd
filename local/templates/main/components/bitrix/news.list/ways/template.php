<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="contacts-ways__tabs">
<? foreach($arResult["ITEMS"] as $key => $arItems) : ?>
<?
$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<?php
if($key == 0){
	$active = 'active';
    $icon = '#map-info-3';
	$dataway = 'auto-way';
}
if($key == 1){
	$active = '';
    $icon = '#map-info-2';
	$dataway = 'train-way';
}
if($key == 2){
	$active = '';
    $icon = '#map-info-4';
	$dataway = 'bus-way';
}
if($key == 3){
	$active = '';
    $icon = '#map-info-1';
	$dataway = 'aero-way';
}          
?>
<div class="contacts-ways__item" id="<?=$this->GetEditAreaId($arItems['ID']);?>">
	<div class="contacts-ways__tab <?=$active?>" data-toggle="contacts-way" data-way="<?=$dataway?>">
		<div class="icon">
			<svg width="28" height="17" fill="none">
				<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg<?=$icon;?>"></use>
			</svg>
		</div>
		<div class="p"><?=$arItems['NAME'];?></div>
	</div>
	<div class="contacts-ways__descr">
		<div class="p small"><?=$arItem['PREVIEW_TEXT']?></div>
		<div class="p">
		<?=$arItem['DETAIL_TEXT']?>
		</div>
		<br>
		<div class="p small"><?=$arItem['PROPERTIES']['TIMETABLE']['VALUE']?></div>
		<div class="row align-items-end justify-content-between">
			<div class="col-auto">
				<div class="p">
				<?=$arItem['DETAIL_TEXT']?>
				</div>
			</div>
			<div class="col-auto">
				<a href="https://rasp.yandex.ru/search/?fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&toName=%D0%9A%D0%BE%D0%B2%D1%80%D0%BE%D0%B2&toId=c10664&when=%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F&transport=train&seats=y&highSpeedTrain=252&highSpeedTrain=260" target="_blank">
					<svg width="20" height="20" fill="#1C9673">
						<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg<?=$icon?>"></use>
					</svg>
					<span><?=$arItem['PROPERTIES']['TIMETABLE']['VALUE']?></span>
				</a>
			</div>
		</div>
	</div>
</div>
<?endforeach;?>
</div>
<div class="contacts-ways__info">
<? foreach($arResult["ITEMS"] as $key => $arItem) : ?>
<?php
if($key == 0){
	$active = 'active';
	$dataway = 'auto-way';
	$way = '';
}
if($key == 1){
	$active = '';
	$dataway = 'train-way';
	$way = '#way';
}
if($key == 2){
	$active = '';
	$dataway = 'bus-way';
	$way = '#way';
}
if($key == 3){
	$active = '';
	$dataway = 'aero-way';
	$way = '';
}          
?> 
<div class="contacts-ways__descr <?=$active;?>" id="<?=$dataway?>">
	<div class="p small"><?=$arItem['PREVIEW_TEXT']?></div>
		<div class="p">
			<?=$arItem['DETAIL_TEXT']?>
		</div>
		<br>
		<div class="p small"><?=$arItem['PROPERTIES']['TITLE']['VALUE']?></div>
		<div class="row align-items-end justify-content-between">
			<div class="col-auto">
				<div class="p">
				<?=$arItem['PROPERTIES']['TITLEDETAIL']['~VALUE']['TEXT']?>
				</div>
			</div>
			<div class="col-auto">
				<a href="https://rasp.yandex.ru/search/?fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&toName=%D0%9A%D0%BE%D0%B2%D1%80%D0%BE%D0%B2&toId=c10664&when=%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F&transport=train&seats=y&highSpeedTrain=252&highSpeedTrain=260" target="_blank">
					<svg width="20" height="20" fill="#1C9673">
						<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg<?=$way?>"></use>
					</svg>
					<span><?=$arItem['PROPERTIES']['TIMETABLE']['VALUE']?></span>
				</a>
			</div>
		</div>
	</div>
<? endforeach; ?>
</div>

