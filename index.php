<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Главная страница");
?>
<main class="main">
<section class="banner banner_top" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/banners/banner-2.jpg);">
    <video class="banner__video mobile" loop="loop" autoplay="" muted="">
        <source src="<?=SITE_TEMPLATE_PATH?>/assets/img/video-banner-mobile.mp4" type="video/mp4"/>
    </video>
    <video class="banner__video desktop" loop="loop" autoplay="" muted="">
        <source src="<?=SITE_TEMPLATE_PATH?>/assets/img/video-banner-2.mp4" type="video/mp4" media="screen"/>
    </video>
    <div class="wrapper">
        <div class="banner__content">
            <div class="banner__title banner__title_small" data-aos-duration="1000" data-aos="zoom-in">ДОБРОГРАД - 1</div>
            <p class="banner__descr" data-aos-duration="1500" data-aos="fade-up">Особая экономическая зона промышленно-производственного типа</p>
            <div class="banner__buttons desktop" data-aos-duration="2000" data-aos="fade-up">
                <div class="button__wrapp">
                    <a href="" class="button button_grey" data-fancybox data-animation-duration="700" data-src="#airport-info" href="javascript:;">
                        <span>Скачать презентацию</span>
                    </a>
                </div>
                <div class="button__wrapp">
                    <a href="/upload/presentation-eng.pdf" target="_blank" class="button button_grey">
                        <span>Download presentation</span>
                    </a>
                </div>
                <div class="button__wrapp">
                    <a href="" class="button button_transparent-grey" data-fancybox data-animation-duration="700" data-src="#resident-request" href="javascript:;">
                        <span>Стать резидентом</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-buttons mobile">
    <div class="wrapper">
        <div class="banner__buttons" data-aos-duration="2000" data-aos="fade-up">
            <div class="button__wrapp">
                <a href="" class="button button_green" data-fancybox data-animation-duration="700" data-src="#airport-info" href="javascript:;">
                    <span>Скачать презентацию</span>
                </a>
            </div>
            <div class="button__wrapp">
                <a href="/upload/presentation-eng.pdf" target="_blank" class="button button_green">
                    <span>Download presentation</span>
                </a>
            </div>
            <div class="button__wrapp">
                <a href="" class="button" data-fancybox data-animation-duration="700" data-src="#resident-request" href="javascript:;">
                    <span>Стать резидентом</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="index-text">
    <div class="wrapper">
        <div class="row mx-n4">
            <div class="col-12 col-md-6 px-4">
                <p>
                    В декабре 2020 года в рамках Владимирского Инвестиционного конгресса Губернатор Владимирской области Владимир Сипягин и основатель ГК «Аскона» Владимир Седов объявили о получении статуса Особой экономической зоны промышленно-производственного типа «Доброград&nbsp;–&nbsp;1» (ОЭЗ&nbsp;ППТ).<br><br>
                    ОЭЗ «Доброград&nbsp;–&nbsp;1» располагается во Владимирской области всего <span class="green">в 5&nbsp;км</span> от города Доброград и занимает площадь <span class="green">211&nbsp;Га.</span>
                </p>
            </div>
            <div class="col-12 col-md-6 px-4">
                <p>
                    Особая экономическая зона «Доброград&nbsp;–&nbsp;1» – это <span class="green">промышленная площадка</span>, на территории которой осуществляется
                    особый режим предпринимательской деятельности, предусматривающий значительный объём преференций. Размещение производства на территории такой промышленно-производственной зоны позволяет повысить конкурентоспособность
                    продукции на российском рынке за счет <span class="green">снижения издержек:</span> налога на имущество, транспортного и земельного налога, налога на прибыль.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="index-activity">
    <div class="wrapper">
        <div class="h2 index-activity__title">Приоритетные направления деятельности</div>
        <div class="index-activity__grid activity">
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-1.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Производство медицинского оборудования</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-2.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Легкая промышленность</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-3.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Производство мебели</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-4.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Химические объекты и производства</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-5.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Среднее машиностроение</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-6.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Производство бумажных изделий и упаковки</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-7.jpg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Производство текстильных изделий</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-8.svg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Производство строительных материалов</div>
                </div>
            </a>
            <a href="" class="activity__item activity-item" data-aos="zoom-in">
                <div class="activity-item__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/activity/activity-9.jpg);"></div>
                <div class="activity-item__content">
                    <div class="activity-item__title">Пищевая промыш&shy;ленность</div>
                </div>
            </a>
        </div>
    </div>
</section>

<section class="index-map" id="location">
    <div class="wrapper">
        <div class="index-map__content">
            <div class="h2 index-map__title active" id="location-title">Удобное расположение</div>
            <div class="h2 index-map__title" id="territory-title">Территория ОЭЗ</div>
            <div class="index-map__tabs">
                <div class="button active" data-toggle="map" data-show="location">
                    <svg width="32" height="32">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#location-map"></use>
                    </svg>
                    <span>Расположение</span>
                </div>
                <div class="button" data-toggle="map" data-show="territory">
                    <svg width="32" height="32">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#territory-map"></use>
                    </svg>
                    <span>Территория ОЭЗ</span>
                </div>
                <div class="button" data-fancybox data-touch="false" data-animation-duration="700" data-src="#route-info" href="javascript:;">
                    <svg width="32" height="32">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#route-map"></use>
                    </svg>
                    <span>Как добраться</span>
                </div>
            </div>
        </div>
    </div>
    <div class="index-map__maps">
        <div class="active" id="location-map"></div>
        <div id="territory-map"></div>
    </div>
</section>

<section class="index-tax">
    <div class="index-tax__section">
        <div class="wrapper">
            <div class="row align-items-center">
                <div class="col-12 col-xl-4">
                    <div class="h2 index-tax__title">Налоговые<br>льготы</div>
                </div>
                <div class="col-12 col-xl-8">
                    <div class="index-tax__block">
                        <svg width="89" height="72" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#percent-0"></use>
                        </svg>
                        <div class="index-tax__list">
                            <div class="index-tax__item">
                                <svg width="40" height="40" fill="none">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#factory"></use>
                                </svg>
                                <p>На имущество</p>
                            </div>
                            <div class="index-tax__item">
                                <svg width="40" height="41" fill="none">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#house"></use>
                                </svg>
                                <p>Земельный</p>
                            </div>
                            <div class="index-tax__item">
                                <svg width="40" height="40" fill="none">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#truc"></use>
                                </svg>
                                <p>Транспортный</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="index-tax__section">
        <div class="wrapper">
            <div class="row align-items-center">
                <div class="col-12 col-xl-4">
                    <div class="h2 index-tax__title">Режим свободной<br>таможенной зоны</div>
                </div>
                <div class="col-12 col-xl-8">
                    <div class="index-tax__block">
                        <svg width="89" height="72" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#percent-0"></use>
                        </svg>
                        <div class="index-tax__list">
                            <div class="index-tax__item">
                                <svg width="40" height="41" fill="none">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#wallet"></use>
                                </svg>
                                <p>НДС</p>
                            </div>
                            <div class="index-tax__item">
                                <svg width="40" height="40" fill="none">
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#tax-officer"></use>
                                </svg>
                                <p>Таможенные пошлины</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="index-tax__section">
        <div class="wrapper">
            <div class="row align-items-center">
                <div class="col-12 col-xl-4">
                    <div class="h2 index-tax__title">Налог<br>на прибыль</div>
                </div>
                <div class="col-12 col-xl-8">
                    <picture>
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/percent-story.svg" media="(min-width: 768px)">
                        <source srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/percent-story-mobile.svg" media="(max-width: 767px)">
                        <img src="" alt="">
                    </picture>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-advantages">
    <div class="wrapper">
        <div class="h2 index-advantages__title">Преимущества ОЭЗ</div>
        <div class="index-advantages__list" data-aos="zoom-out">

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#map"></use>
                            </svg>
                        </div>
                        <div class="text">Выгодная локация и транспортная доступность. Гибкие логистические решения. Наличие собственного аэропорта</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Выгодная локация и транспортная доступность</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="58" height="30" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#percent"></use>
                            </svg>
                        </div>
                        <div class="text">Налоговые и таможенные льготы</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Налоговые и таможенные льготы</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#team"></use>
                            </svg>
                        </div>
                        <div class="text">Ресурсные возможности с минимально возможными тарифами</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Ресурсы с минимальными тарифами</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#managers"></use>
                            </svg>
                        </div>
                        <div class="text">Кадровый потенциал региона с традиционно промышленной ментальностью учебных заведений</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Кадровый потенциал</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#engineer"></use>
                            </svg>
                        </div>
                        <div class="text">Низкая стоимость труда по сравнению с другими ОЭЗ</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Низкая стоимость труда</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#rent"></use>
                            </svg>
                        </div>
                        <div class="text">Готовая инфраструктура для жизни, работы и отдыха в аренду и на продажу</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Готовая инфраструктура для жизни</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#city"></use>
                            </svg>
                        </div>
                        <div class="text">Готовая бизнес-инфраструктура с возможностью аренды</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Готовая бизнес-инфраструктура</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#crane"></use>
                            </svg>
                        </div>
                        <div class="text">Многолетний опыт строительства промышленных объектов «под ключ»</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Большой опыт строительства</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#professional"></use>
                            </svg>
                        </div>
                        <div class="text">Экспертность управляющей компании с опытом реализации международных проектов. Уникальный спектр услуг для резидентов</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Экспертная управляющая компания</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

            <div class="advantages-item">
                <div class="advantages-item__content">
                    <a href="" class="visible">
                        <div class="icon">
                            <svg width="64" height="64" fill="none">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#managers"></use>
                            </svg>
                        </div>
                        <div class="text">Режим «дружественного администрирования» и персональный менеджер вашего проекта</div>
                    </a>
                    <div class="hidden">
                        <div class="title">Дружественное администрирование</div>
                        <div class="text">Гибкие логистические решения. Наличие собственного аэропорта малой авиации и парка воздушных судов</div>
                        <a href="#" class="button button_green"><span>подробнее</span></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="advendages-slider">
<div class="wrapper">
<div class="h2 advendages-slider__title">Преимущества ОЭЗ</div>
<div class="swiper-container advendages-slider__slider">
    <div class="swiper-wrapper">

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#map"></use>
                        </svg>
                    </div>
                    <div class="text">Выгодная локация и транспортная доступность. Гибкие логистические решения. Наличие собственного аэропорта</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="58" height="30" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#percent"></use>
                        </svg>
                    </div>
                    <div class="text">Налоговые и таможенные льготы</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#map"></use>
                        </svg>
                    </div>
                    <div class="text">Ресурсы с минимальными тарифами</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#gauge"></use>
                        </svg>
                    </div>
                    <div class="text">Кадровый потенциал</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#engineer"></use>
                        </svg>
                    </div>
                    <div class="text">Низкая стоимость труда</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#rent"></use>
                        </svg>
                    </div>
                    <div class="text">Готовая инфраструктура для жизни</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#city"></use>
                        </svg>
                    </div>
                    <div class="text">Готовая бизнес-инфраструктура</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#crane"></use>
                        </svg>
                    </div>
                    <div class="text">Большой опыт строительства</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#professional"></use>
                        </svg>
                    </div>
                    <div class="text">Экспертная управляющая компания</div>
                </a>
            </div>
        </div>

        <div class="advantages-item swiper-slide">
            <div class="advantages-item__content">
                <a href="" class="visible">
                    <div class="icon">
                        <svg width="64" height="64" fill="none">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite.svg#managers"></use>
                        </svg>
                    </div>
                    <div class="text">Дружественное администрирование</div>
                </a>
            </div>
        </div>
    </div>
</div>        
</div>
</section>

<section class="question">
    <div class="wrapper">
        <div class="question__content" data-aos="zoom-in-up">
            <div class="question__block">
                <div class="h2 question__title">Хотите стать резидентом ?</div>
                <div class="question__buttons">
                    <!-- <a href="" class="button button_green"><span>Требования и шаги</span></a> -->
                    <a href="" class="button" data-fancybox data-animation-duration="700" data-src="#consult-request" href="javascript:;"><span>Получить консультацию</span></a>
                </div>
            </div>
            <div class="question__picture" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/img/welcome.jpg);"></div>
        </div>
    </div>
</section>
</main>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>